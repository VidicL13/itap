
# uporabimo metriko Fbeta
customPenalty <- function(data, levels, ...){
  # PPV = TP/(TP+FP)
  # TPR = TP/P
  # Fb = (1+b^2)*(PPV*TPR)/(b^2*PPV+TPR)
  beta = 2
  D <- levels[1]
  N <- levels[2]
  TP <- sum(data$pred == D & data$obs == D)
  P <- sum(data$obs == D)
  FP <- sum(data$pred == D & data$obs == N)
  PPV <- TP/(FP+TP)
  TPR <- TP/P
  cost = (1+beta^2)*(PPV*TPR)/((beta^2)*PPV+TPR)
  names(cost) <- 'Fbeta'
  return(cost)
}

## definiramo nekaj "Koncnih" modelov, glede na metodo (knn,svm,rpart in rf (RandomForest)) ####

modelKoncenKnn<- train(Y ~ ., data = Data,
                       method = "knn",
                       metric = "Fbeta",
                       maximize = "true",
                       trControl = trainControl(method = "cv",
                                                number = 10,
                                                savePredictions = TRUE,
                                                classProbs = TRUE,
                                                summaryFunction = customPenalty),
                       tuneGrid = data.frame(k=1:20))
modelKoncenKnn$results
 
modelKoncenSvm <- train(Y ~ ., data = Data,
                        method = "svmLinear",
                        metric = "Fbeta",
                        maximize = "true",
                        trControl = trainControl(method = "cv",
                                                 number = 10,
                                                 savePredictions = TRUE,
                                                 classProbs = TRUE,
                                                 summaryFunction = customPenalty),
                        tuneGrid = data.frame(C=c(0.001,1,100)))
modelKoncenSvm$results