library("caret")
N<-200

X1 <- rnorm(N, mean=1)
X2 <- rnorm(N, mean=1)
X3 <- rnorm(N, mean=1)
X4 <- rnorm(N, mean=1)
X5 <- rnorm(N, mean=1)


Y <- 8 + 5*X1 + X2 + X3*X4 + X5*X5+ X1*X2*X3 + rnorm(N)


myData <- data.frame(X1=X1, X2=X2, X3=X3, X4=X4, X5=X5, Y=Y)

tc <- trainControl(method="none")
linearModel <- train(Y~., data=myData, method="glm", trControl=tc)


#kako da napraveme kvadratni faktori
# imame dva nachini

squareData <- myData
squareData$X11 <- X1*X1
squareData$X12 <- X1*X2
squareData$X13 <- X1*X3
squareData$X14 <- X1*X4
squareData$X15 <- X1*X5
squareData$X22 <- X2*X2
squareData$X23 <- X2*X3
squareData$X24 <- X2*X4
squareData$X25 <- X2*X5
squareData$X33 <- X3*X3
squareData$X34 <- X3*X4
squareData$X35 <- X3*X5
squareData$X44 <- X4*X4
squareData$X45 <- X4*X5
squareData$X55 <- X5*X5

squareModel <- train(Y~., data=squareData, method="glm", trControl=tc)


# second approach
squareData <- data.frame(poly(as.matrix(myData[,1:5]), degree = 2, raw=TRUE), y=myData$Y)
------------------------------------------------------------
  # 2.-------------

train(Y~X1+I(X1*X1), data = squareData) # to make the formula X1^2, X1*X1 means interaction and it is same as X1 <=> X1*X1

library(gtools)
features <- names(myData)[names(myData)!="Y"]
co <- combinations(5, 2, v=features,  repeats.allowed=TRUE)

p1 <- apply(co, 1, paste, colapse="*")
p2 <- sapply(p1, function(x){paste0('I(', x, ')')})

