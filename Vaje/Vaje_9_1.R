library(caret)
library(mxnet)
library(devtools)
library(DiagrammeR)
library(mlbench)

data <- read.csv('./Podatki/train.csv')

trainIndex <- createDataPartition(data$label, p=0.9, list = FALSE)
trainX <- data.matrix(data[trainIndex, names(data) != 'label'])
trainY <- data$label[trainIndex]

trainX <- trainX / 255

testX <- data.matrix(data[-trainIndex, names(data) != 'label'])
testY <- data$label[-trainIndex]
testX <- testX / 255

dataLayer <- mx.symbol.Variable("data")
hiddenLayer1 <- mx.symbol.FullyConnected(data = dataLayer, num.hidden = 128, name = 'Plast 1') 
activation1 <- mx.symbol.Activation(data = hiddenLayer1, act.type='tanh')
hiddenLayer2 <- mx.symbol.FullyConnected(data = activation1, num.hidden = 65, name = 'Plast 2')
activation2 <- mx.symbol.Activation(data = hiddenLayer2, act.type='tanh')
hiddenLayer3 <- mx.symbol.FullyConnected(data = activation2, num.hidden = 10, name = 'Plast 3')
finalNetwork <- mx.symbol.SoftmaxOutput(data = hiddenLayer3)

graph.viz(finalNetwork)

fcModel <- mx.model.FeedForward.create(
  finalNetwork, X = trainX, y = trainY,
  num.round = 10,
  learning.rate = 0.05,
  momentum = 0.9,
  eval.metric = mx.metric.accuracy,
  initializer = mx.init.uniform(0.01),
  batch.end.callback = mx.callback.log.train.metric(128),
  array.batch.size = 128,
  ctx = mx.cpu()
)






