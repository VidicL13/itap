library(caret)
library(kernlab)
library(grid)
source('plotAreas.R')

# Generiranje radialno ločljivih podatkov

N <- 200
sd <- 1
r <- c(rnorm(N/2, mean = 2, sd = sd), rnorm(N/2, mean = 4, sd = sd))
theta <- runif(N)
X1 <- r * cos(2 * pi * theta)
X2 <- r * sin(2 * pi * theta)
Y <- c(rep(TRUE, N/2), rep(FALSE, N/2)) %>% as.factor()
myData <- data.frame(X1 = X1, X2 = X2, Y = Y)

plot(X1[Y==TRUE], X2[Y==TRUE], col='red', ylim=c(min(X2), max(X2)), xlim=c(min(X1), max(X1)))
points(X1[Y==FALSE], X2[Y==FALSE], col='blue')

myModel <- train(Y~., data = myData,
                 method = 'svmRadial',
                 tuneGrid = data.frame(sigma = 1, C=Inf),
                 trControl = trainControl(method = 'none'))

plotAreas(myModel, myData, gridNum = 100)
plot(myModel$finalModel, data=as.matrix(myData[names(myData)!='Y']))

# Dodamo šum

Cs <- 10^seq(-3,10,3)
  
myNoisyModel <- train(Y~., data = myData,
                      method = 'svmRadial',
                      trControl = trainControl(method = 'cv', number = 10),
                      tuneGrid = data.frame(sigma = 1, C=Cs))
myNoisyModel

# Narišemo

for(C in Cs){
  tmpModel <- train(Y~., data = myData,
                        method = 'svmRadial',
                        trControl = trainControl(method = 'none'),
                        tuneGrid = data.frame(sigma = 1, C=C))
  plt <- plotAreas(tmpModel, myData, gridNum = 200)
  plot(plt)
}
plot(tmpModel$finalModel, data=as.matrix(myData[names(myData)!='Y']))



