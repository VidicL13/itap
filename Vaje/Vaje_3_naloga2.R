library("caret")
N<-200

X1 <- rnorm(N, mean=1)
X2 <- rnorm(N, mean=1)
X3 <- rnorm(N, mean=1)
X4 <- rnorm(N, mean=1)
X5 <- rnorm(N, mean=1)

Y <- 8 + 5*X1 + X2 + X3*X4 + X5*X5 + X1*X2*X3 + rnorm(N)

myData <- data.frame(X1=X1, X2=X2, X3=X3, X4=X4, X5=X5, Y=Y)

squareData <- data.frame(poly(as.matrix(myData[,1:5]), degree = 2, raw = TRUE), Y=myData$Y)

tc <- trainControl(method = "CV", number = 10, index = createFolds(myData$Y, k=10, returnTrain = TRUE))

linearModel <- train(Y~., data = myData, method = "glm", trControl = tc)
squareModel <- train(Y~., data = squareData, method = "glm", trControl = tc)




