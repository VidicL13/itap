library("caret")
library("dplyr")
data(iris)

TrainIndex <- createDataPartition(iris$Species, p = 0.8, list = FALSE)
TrainData <- iris[TrainIndex,]

# testne množice v modelu ne uporabljamo, uporabimo jo le za testiranje
TestData <- iris[-TrainIndex,]

knnModel <- train(Species ~ ., TrainData, method = "knn")
knnModel$finalModel

# poglejmo še predikcijo Species
Y <- TrainData$Species
TrainData$Species <- NULL
knnModel2 <- train(TrainData, Y, method = "knn")

knnModel3 <- train(Species ~ ., TrainData, method = "knn", tuneGrid = data.frame(k = 1:30))
plot(knnModel3)
knnModel3$results

# Preverimo 
predictions <- predict(knnModel3, newdata = TestData)
err <- sum(predictions != TestData$Species) / length(TestData$Species)


# Drugi del nalogo
# uvoz podatkov
Data <- read.csv(file = "./Podatki/podatki.csv", fileEncoding = "UTF-8")
Data$X <- NULL
Data$X4[Data$X4 == 'NO'] <- 'NE'
Data$X4[Data$X4 == 'YES'] <- 'DA'
Data$X4[Data$X4 == '“NE”'] <- 'NE'
Data$X4[Data$X4 == '“DA”'] <- 'DA'

Data$X4 <- droplevels(Data$X4)

Data$X6 <- gsub('“', '', as.character(Data$X6))
Data$X6 <- gsub('”', '', as.character(Data$X6))
Data$X6 <- as.numeric(Data$X6)

# ta del smo pogruntal kasneje
Data$X2 <- Data$X2/100
Data$X3 <- Data$X3/20
Data$X5 <- Data$X5/100
Data$X6 <- NULL

TrainIndex1 <- createDataPartition(Data$Y, p = 0.8, list = FALSE)
TrainData1 <- Data[TrainIndex1, ]
TrainData1$Y <- as.factor(TrainData1$Y)
TestData1 <- Data[-TrainIndex1, ]

knnModel4 <- train(Y ~ ., data = TrainData1, method = 'knn')
predictions1 <- predict(knnModel4, newdata = TestData1)
err1 <- sum(predictions1 != TestData1$Y) / length(TestData1$Y)
plot(knnModel4)




